<?php  
include_once('transporte.php');

	//declaracion de la clase hijo o subclase Carro
class tren extends transporte{

	private $tipo;

	//declaracion de constructor
	public function __construct($nom,$vel,$com,$tipo){
		//sobreescritura de constructor de la clase padre
		parent::__construct($nom,$vel,$com);
		$this->tipo=$tipo;
			
	}

	// declaracion de metodo
	public function resumenTren(){
		// sobreescribitura de metodo crear_ficha en la clse padre
		$mensaje=parent::crear_ficha();
		$mensaje.='<tr>
					<td>Tipo:</td>
					<td>'. $this->tipo.'</td>				
				</tr>';
		return $mensaje;
	}
} 

?>