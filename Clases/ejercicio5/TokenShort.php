<?php

class TokenShort {

  private $nombre;
  private $token = '';
  private const TOKEN_SIZE = 4;

  public function __construct($nombre) {
    $this->nombre = $nombre;
    $this->token = $this->tokenGenerator();
  }

  private function tokenGenerator() {
    $nums = [self::TOKEN_SIZE];

    // generamos numeros en un rango de 65 - 90
    // que en codigo ascii corresponden a las 
    // letras mayusculas del abecedario 
    // inmediatamente convertimos ese numero
    // a un caracter.
    for($i = 0; $i < self::TOKEN_SIZE; $i++) {
      $nums[$i] = rand(65, 90);
      $nums[$i] = chr($nums[$i]);
    }

    return implode($nums);
  }

  public function __destruct() {
    echo '<div class="container col-5 alert alert-success text-center mt-4 "> Tu token: ' . $this->token . '</div>';
  }
}

if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new TokenShort($_POST['nombre']);
}


?>