<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto {
	public $marca;
	public $color;
}


//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidorMoto='';

$miMoto = new Moto();

if ( !empty($_POST)){

	$miMoto->marca = $_POST['marca'];
	$miMoto->color = $_POST['color-moto'];

	$mensajeServidorMoto .= "La marca de la moto es '" . $miMoto->marca . "' y su color es " . $miMoto->color;
}  

?>
