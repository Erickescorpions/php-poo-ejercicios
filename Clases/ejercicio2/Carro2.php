<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anioFabricacion;

	private $estadoVerificacion; 

	//declaracion del método verificación
	public function verificacion(){
		if($this->anioFabricacion < '1990-01') {
			$this->estadoVerificacion = 'No';
		} else if($this->anioFabricacion < '2010-01') {
			$this->estadoVerificacion = 'Revision';
		} else {
			$this->estadoVerificacion = 'Si';
		}
	}

	public function getEstadoVerificacion() {
		return $this->estadoVerificacion;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anioFabricacion = $_POST['anio'];
	$Carro1->verificacion();
}